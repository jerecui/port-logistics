package com.jere;

/**
 * Means on unit
 */
public class Unit {
    private Object object;

    public Unit() {

    }

    public Unit(Object object) {
        this.load(object);
    }

    public void load(Object object) {
        this.object = object;
    }

    public Object unload() {
        var obj = this.object;
        this.object = null;

        return obj;
    }

    @Override
    public String toString() {
        if (this.object == null) return " ";

        return this.object.toString();
    }

    public boolean isEmpty() {
        return this.object == null;
    }
}
