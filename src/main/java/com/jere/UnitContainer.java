package com.jere;

import java.util.ArrayList;
import java.util.List;

public class UnitContainer {
    private List<Unit> units;

    public List<Unit> getUnits() {
        return this.units;
    }

    public UnitContainer(int capacity) {
        if (capacity > 0) {
            units = new ArrayList<>();
            for (int i = 0; i < capacity; i++) {
                units.add(new Unit());
            }
        }
    }

    public void loadOne(Object object) {
        for (Unit unit : units) {
            if (unit.isEmpty()) {
                unit.load(object);
                break;
            }
        }
    }

    public Object unloadOne() {
        for (int i = units.size() - 1; i >= 0; i--) {
            var unit = units.get(i);
            if (!unit.isEmpty()) {
                return unit.unload();
            }
        }

        return null;
    }

    public void loadEach(Object object) {
        for (Unit unit : units) {
            unit.load(object);
        }
    }

    public Boolean isFull() {
        for (Unit unit: units) {
            if (unit.isEmpty()) {
                return false;
            }
        }
        return true;
    }

    public Boolean isEmpty() {
        for (Unit unit: units) {
            if (!unit.isEmpty()) {
                return false;
            }
        }
        return true;
    }

    public int availableCount() {
        return (int) this.units.stream().filter(Unit::isEmpty).count();
    }

    public int unitCount() {
        return (int) this.units.stream().filter(x -> !x.isEmpty()).count();
    }
}
