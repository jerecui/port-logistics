package com.jere;

public class Port {
    private Ship ship;
    private Storage storage;
    private Train train;

    public Port() {
        this.storage = new Storage();
        this.train = new Train();
    }

    public String show() {
        var sb = new StringBuilder();
        sb.append("-^-^");
        appendLine(sb);

        for (int unitIndex = this.storage.getUnits().size() - 1; unitIndex >=0 ; unitIndex--) {
            // ship
            if (this.ship == null || unitIndex >= this.ship.getUnits().size()) {
                sb.append(" ");
            } else {
                sb.append(this.ship.getUnits().get(unitIndex).toString());
            }

            // storage
            sb.append("|");
            sb.append(storage.getUnits().get(unitIndex).toString());
            sb.append("|");

            if (unitIndex > 0) appendLine(sb);
        }

        for (var unit: this.train.getUnits()) {
            sb.append(unit.toString());
        }

        sb.append("  D i");
        appendLine(sb);

        // last line for ship.
        if (this.ship == null) sb.append(" ");
        else sb.append("V");

        sb.append("A_A---::%%%");
        return sb.toString();
    }

    public void receiveShip() {
        var ship = new Ship();
        ship.loadEach("X");
        this.ship = ship;
    }

    public void unload() {
        if (this.ship == null) {
            // move storage to train
            if (!this.train.isFull()) {
                moveStorageToTrain();
            }
        } else {
            while ((!this.train.isFull() && (this.ship.unitCount()> 0 || this.storage.unitCount() > 0))
            || this.train.isFull() && !this.storage.isFull() && this.ship.unitCount()> 0) {
                moveShipToStorage();
                moveStorageToTrain();
            }
        }
    }

    private void moveStorageToTrain() {
        var count = this.train.availableCount();
        for (int i = 0; i <count; i++) {
            var obj = this.storage.unloadOne();
            if (obj != null) {
                this.train.loadOne(obj);
            }
        }
    }

    private void moveShipToStorage() {
        var count = this.storage.availableCount();
        for (int i = 0; i <count; i++) {
            var obj = this.ship.unloadOne();
            if (obj != null) {
                this.storage.loadOne(obj);
            }
        }
    }

    public void trainSend() {
        this.train = new Train();
    }

    private void appendLine(StringBuilder sb) {
        if (sb != null) sb.append("\n");
    }
}
