package com.jere;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TrainTest{
    @Test
    public void can_init_with_capacity() {
        var storage = new Train();
        var units = storage.getUnits();
        assertEquals(3, units.size());
        for (var unit : units) {
            assertTrue(unit.isEmpty());
        }
    }
}