package com.jere;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PortTest {
    @Test
    public void when_port_is_empty() {
        var port = new Port();
        assertEquals("-^-^\n" +
                " | |\n" +
                " | |\n" +
                " | |\n" +
                " | |\n" +
                " | |     D i\n" +
                " A_A---::%%%", port.show());
    }

    @Test
    public void receive_ship_and_show() {
        var port = new Port();
        port.receiveShip();
        assertEquals("-^-^\n" +
                " | |\n" +
                "X| |\n" +
                "X| |\n" +
                "X| |\n" +
                "X| |     D i\n" +
                "VA_A---::%%%", port.show());
    }

    @Test
    public void receive_ship_and_unload() {
        var port = new Port();
        port.receiveShip();
        port.unload();
        assertEquals("-^-^\n" +
                " | |\n" +
                " | |\n" +
                " | |\n" +
                " | |\n" +
                " |X|XXX  D i\n" +
                "VA_A---::%%%", port.show());
    }

    @Test
    public void receive_ship_and_unload_twice() {
        var port = new Port();
        port.receiveShip();
        port.unload();
        port.receiveShip();
        port.unload();
        assertEquals("-^-^\n" +
                " |X|\n" +
                " |X|\n" +
                " |X|\n" +
                " |X|\n" +
                " |X|XXX  D i\n" +
                "VA_A---::%%%", port.show());
    }

    @Test
    public void receive_ship_and_unload_tripple_and_send_train() {
        var port = new Port();
        port.receiveShip();
        port.unload();
        port.receiveShip();
        port.unload();
        port.receiveShip();
        port.unload();
        port.trainSend();
        assertEquals("-^-^\n" +
                " |X|\n" +
                "X|X|\n" +
                "X|X|\n" +
                "X|X|\n" +
                "X|X|     D i\n" +
                "VA_A---::%%%", port.show());
    }
}