package com.jere;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class StorageTest {
    @Test
    public void can_init_with_capacity() {
        var storage = new Storage();
        var units = storage.getUnits();
        assertEquals(5, units.size());
        for (var unit : units) {
            assertTrue(unit.isEmpty());
        }
    }
}