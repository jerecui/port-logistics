package com.jere;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class UnitTest {
    @Test public void should_get_string_if_the_unit_has_value() {
        var unit = new Unit();
        unit.load("X");
        assertEquals("X", unit.toString());
    }

    @Test public void should_get_empty_string_if_the_unit_has_no_value() {
        var unit = new Unit();
        assertEquals(" ", unit.toString());
    }

    @Test public void when_contains_should_not_be_empty() {
        var unit = new Unit();
        assertTrue(unit.isEmpty());
        unit.load("X");
        assertFalse(unit.isEmpty());
    }

}