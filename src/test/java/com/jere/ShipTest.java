package com.jere;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class ShipTest {
    @Test
    public void can_init_with_capacity() {
        var ship = new Ship();
        var units = ship.getUnits();
        assertEquals(4, units.size());
        for (var unit : units) {
            assertTrue(unit.isEmpty());
        }
    }

    @Test
    public void load_one() {
        var ship = new Ship(2);

        ship.loadOne("X");
        var units = ship.getUnits();

        assertFalse(units.get(0).isEmpty());
        assertTrue(units.get(1).isEmpty());
    }

    @Test
    public void load_two() {
        var ship = new Ship(2);

        ship.loadOne("X");
        ship.loadOne("X");
        var units = ship.getUnits();

        assertFalse(units.get(0).isEmpty());
        assertFalse(units.get(1).isEmpty());
    }

    @Test
    public void load_if_great_than_capacity() {
        var ship = new Ship(2);
        ship.loadOne("X");
        ship.loadOne("X");
        ship.loadOne("X");
        var units = ship.getUnits();

        assertFalse(units.get(0).isEmpty());
        assertFalse(units.get(1).isEmpty());
    }

    @Test
    public void unload_one_if_has_one() {
        var ship = new Ship(2);
        ship.loadOne("X");
        var unit = ship.unloadOne();
        assertEquals("X", unit);

        var units = ship.getUnits();
        assertTrue(units.get(0).isEmpty());
        assertTrue(units.get(1).isEmpty());
    }

    @Test
    public void unload_one_if_has_two() {
        var ship = new Ship(2);
        ship.loadOne("X");
        ship.loadOne("X");
        ship.unloadOne();

        var units = ship.getUnits();
        assertFalse(units.get(0).isEmpty());
        assertTrue(units.get(1).isEmpty());
    }

    @Test
    public void loadEach() {
        var ship = new Ship(2);
        ship.loadEach("X");

        for (Unit unit : ship.getUnits()) {
            assertFalse(unit.isEmpty());
        }
    }

    @Test
    public void isFull_isEmpty() {
        var ship = new Ship(2);
        ship.loadEach("X");
        assertTrue(ship.isFull());
        assertFalse(ship.isEmpty());

        ship.unloadOne();
        assertFalse(ship.isFull());
        assertFalse(ship.isEmpty());

        ship.unloadOne();
        assertTrue(ship.isEmpty());
    }
}